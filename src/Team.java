
import java.io.File;
import java.io.FileInputStream;
import java.io.PrintStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Properties;

import javax.imageio.event.IIOReadWarningListener;
import javax.sound.sampled.LineListener;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Team {
	public static void main(String[] args) throws Exception {
		int i,k=0,max,min;
		String name=null,num=null;
		double exp=0;
		int avg = 0;
		String[] arrUrl = new String[99];//定义一个存储各个课堂完成部分url的数组
		Properties pps = new Properties();
		pps.load(new FileInputStream("resources\\config.properties"));
		String url = pps.getProperty("url");//获取云班的URL
		String cookie = pps.getProperty("cookie");//通过cookie进行模拟登陆
		List list = new ArrayList();
		//解析云班课活动主页
		Document document =Jsoup.connect(url).header("Cookie", cookie).timeout(30000).maxBodySize(0).get();
		//获取成员数
		Elements member=document.getElementsByClass("fontsize-12");
		int members=Integer.parseInt(member.get(1).text().replaceAll("[()]", ""));
		//通过循环获取各个课堂完成部分的url
		Elements elements = document.getElementsByClass("interaction-row");
		for (i=0;i<elements.size();i++) {
			if (elements.get(i).toString().contains("课堂完成")) {
//				System.out.println(k+" "+elements.get(i).attr("data-url"));//检查是否是课堂完成部分
				arrUrl[k]=(String)elements.get(i).attr("data-url");
				k++;//课堂完成部分总数
			}
		} 
		//获取每个人的经验
		for (i = 0; i < k; i++) {
			Document document2=Jsoup.connect(arrUrl[i]).header("Cookie", cookie).timeout(30000).maxBodySize(0).get();//解析课堂完成部分页面
			Elements elements2=document2.getElementsByClass("homework-item");//获取每个人作业的div模块
			for (int m = 0; m < elements2.size() ; m++) {
				try {
					if(elements2.get(m).child(3).child(1).text().replaceAll("[最终得分：]", "")!=null) {
						exp=Double.parseDouble(elements2.get(m).child(3).child(1).text().replaceAll("[最终得分：]", ""));//检查是否是获取经验
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
				name=elements2.get(m).child(0).child(1).child(0).text();
				num=elements2.get(m).child(0).child(1).child(1).text();
				Student s=new Student(name, num, exp);
				if (list.contains(s)) {
					for (int a = 0; a < list.size(); a++) {
						Student s1 = (Student) list.get(a);
						if (s1.getName().equals(name)) {
							s1.setExp(s1.getExp()+exp);
						}
					}
				}
				else {
					list.add(new Student(name, num, exp));
				}
			}
		}
		 list.sort(new Comparator<Student>() {
	            public int compare(Student o1, Student o2) {
	                return  (int) (o2.getExp()-o1.getExp());
	            }
	        });
		for (i = 0; i < list.size(); i++) {
			Student student = (Student) list.get(i);
			avg+=student.getExp();
		}
		avg = avg/list.size();
		String avgString = String.format("%d",avg);
		Student stumax = (Student) list.get(0);
		Student stumin = (Student) list.get(list.size()-1);
		File file = new File("Score.txt");
		PrintStream pts = new PrintStream(file);
		pts.println("最高经验值：" +(int)stumax.getExp() + "，最低经验值：" +(int)stumin.getExp() + "，平均经验值：" + avgString);
		for (i = 0; i < list.size(); i++) {
			Student student = (Student) list.get(i);
            pts.println(student.getNum()+","+student.getName()+","+(int)student.getExp());
        }
		pts.close();
	}
}
